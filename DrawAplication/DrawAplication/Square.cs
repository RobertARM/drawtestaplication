﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawAplication
{
    class Square :Point
    {
        public Square()
        {
            width = 8;
            symbol = '*';

            color = Console.ForegroundColor;
            fonColor = Console.BackgroundColor;
        }

        public void Draw()
        {
            Console.ForegroundColor = color;
            Console.BackgroundColor = fonColor;
            height = width;

            Line();
            Console.WriteLine();
            HeightLine();
            Line();

            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;
        }
    }
}
