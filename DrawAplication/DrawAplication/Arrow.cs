﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawAplication
{
    class Arrow : Pike
    {
        public Arrow()
        {
            width = 8;
            symbol = '*';

            color = Console.ForegroundColor;
            fonColor = Console.BackgroundColor;
        }
        
        public void Draw()
        {
            Console.ForegroundColor = color;
            Console.BackgroundColor = fonColor;

            Pike1();
            Line();
            Console.WriteLine();
            Pike2();

            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;
        }
    }
}
