﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawAplication
{
    class Rectangle : Point
    {
        public Rectangle()
        {
            width = 8;
            height = 10;
            symbol = '*';

            color = Console.ForegroundColor;
            fonColor = Console.BackgroundColor;
        }

        public void Draw()
        {
            Console.ForegroundColor = color;
            Console.BackgroundColor = fonColor;

            Line();
            Console.WriteLine();
            HeightLine();
            Line();

            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;
        }
    }
}
