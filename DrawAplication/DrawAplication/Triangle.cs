﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawAplication
{
    class Triangle : Point
    {

        public Triangle()
        {
            width = 8;
            symbol = '*';

            color = Console.ForegroundColor;
            fonColor = Console.BackgroundColor;
        }

        public void Draw()
        {
            Console.ForegroundColor = color;
            Console.BackgroundColor = fonColor;


            Console.WriteLine(symbol);
            for (int i = 1; i < width; i++)
            {
                Console.Write(symbol);

                for (int x = 1; x < i*2; x++)
                    Console.Write(" ");

                Console.WriteLine(symbol);

            }
            Line();
            Console.Write(symbol);


            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;
        }
    }

}