﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawAplication
{
    class Program
    {
        static void Main(string[] args)
        {

            Triangle tangle = new Triangle()
            {
                width = 12,
                color = ConsoleColor.Red,
                //fonColor = ConsoleColor.Gray
            };
            tangle.Draw();

            Console.WriteLine();//----------------------------------

            Rectangle rect = new Rectangle()
            {
                width = 10,
                height = 14,
                color = ConsoleColor.Blue,
            };
            rect.Draw();

            Console.WriteLine();//----------------------------------

            Square square = new Square()
            {
                width = 10,
                color = ConsoleColor.Yellow,
            };
            square.Draw();

            Console.WriteLine();//----------------------------------

            Line line = new Line()
            {
                width = 20,
                color = ConsoleColor.DarkRed
            };
            line.Draw();

            Console.WriteLine();//----------------------------------

            Arrow arrow = new Arrow()
            {
                width = 10,
                color = ConsoleColor.Magenta,
            };
            arrow.Draw();

            Console.WriteLine();//----------------------------------

            //ArrowX2 arrow2 = new ArrowX2()
            //{
            //    width = 10
            //};
            //arrow2.Draw();
        }
    }
}
