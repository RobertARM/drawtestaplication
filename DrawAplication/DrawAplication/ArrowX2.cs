﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawAplication
{
    class ArrowX2 : Pike
    {
        public ArrowX2()
        {
            width = 8;
            symbol = '*';

            color = Console.ForegroundColor;
            fonColor = Console.BackgroundColor;
        }

        public void Draw()
        {
            
            Console.ForegroundColor = color;
            Console.BackgroundColor = fonColor;

            //PikeS2();
            //Pike1();
            //Line();
           
            //Console.WriteLine();
            //PikeS1();

            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;
        }
    }
}
